*** Settings ***
Documentation     Android Web Session
Resource          ../resources/resource.robot
Force Tags        Android    Session

*** Test Cases ***
Should Create and Destroy Android Web Session
    Open Application    ${APPIUM_SERVER}    automationName=${ANDROID_AUTOMATION_NAME}
    ...    platformName=${ANDROID_PLATFORM_NAME}    platformVersion=${ANDROID_PLATFORM_VERSION}
    ...    browserName=Chrome
    Go To Url    https://www.google.com
    ${page_title}    execute script    return document.title
    Should Be Equal    ${page_title}    Google
    Close Application
    BuiltIn.Sleep    3s
    Run Keyword And Expect Error    No application is open    execute script    return document.title
