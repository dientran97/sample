*** Settings ***
Documentation     Simple example using AppiumLibrary with real device cloud
Library           AppiumLibrary
Force Tags        Android    BrowserStack

#*** Variables ***
#${ANDROID_APP}    bs://977e6f602c05c041b350475f478452d054c3f193
#${OS_VERSION}     9.0
#${DEVICE_NAME}    Google Pixel 3
#${BROWSERSTACK_SERVER}    https://${BROWSERSTACK_USERNAME}:${BROWSERSTACK_ACCESSKEY}@hub-cloud.browserstack.com/wd/hub
#${BROWSERSTACK_USERNAME}    hangtrinh1
#${BROWSERSTACK_ACCESSKEY}    oYskxo5zqBVPnT38cszZ

*** Test Cases ***
Should send keys to search box and then check the value
    Open Test Application
    BuiltIn.Sleep    30s
    Click App Menu
#    BuiltIn.Sleep    10s
    Click Search Menu
#    BuiltIn.Sleep    10s
    Click Invoke Search Menu
#    BuiltIn.Sleep    10s
    Input Search Query    Hello World!
#    BuiltIn.Sleep    10s
    Submit Search
#    BuiltIn.Sleep    10s
    Search Query Should Be Matching    Hello World!
#    BuiltIn.Sleep    10s
    Close Application

*** Keywords ***
Open Test Application
    Open Application    ${BROWSERSTACK_SERVER}    device=${DEVICE_NAME}    app=${ANDROID_APP}    os_version=${OS_VERSION}
    ...    realMobile=${True}    browserstack.appium_version=1.18.0

Click App Menu
    Click Element    accessibility_id=App

Click Search Menu
    Click Element    accessibility_id=Search

Click Invoke Search Menu
    Click Element    accessibility_id=Invoke Search

Input Search Query
    [Arguments]    ${query}
    Input Text    id=txt_query_prefill    ${query}

Submit Search
    Click Element    btn_start_search

Search Query Should Be Matching
    [Arguments]    ${text}
    Wait Until Page Contains Element    android:id/search_src_text
    Element Text Should Be    android:id/search_src_text    ${text}
