*** Settings ***
Documentation     Simple example using AppiumLibrary
Library           AppiumLibrary
Force Tags        Android    Example

*** Variables ***
${APPIUM_SERVER}    http://192.168.75.145:4723/wd/hub
${ANDROID_AUTOMATION_NAME}    UIAutomator2
${ANDROID_APP}    D:/Data/demoapp/ApiDemos-debug.apk
${ANDROID_PLATFORM_NAME}    Android
${ANDROID_PLATFORM_VERSION}    %{ANDROID_PLATFORM_VERSION=8.0}

*** Test Cases ***
Should send keys to search box and then check the value
    Open Test Application
    Input Search Query    Hello World!
    Submit Search
    Search Query Should Be Matching    Hello World!

*** Keywords ***
Open Test Application
    Open Application    ${APPIUM_SERVER}    automationName=${ANDROID_AUTOMATION_NAME}
    ...    platformName=${ANDROID_PLATFORM_NAME}    platformVersion=${ANDROID_PLATFORM_VERSION}
    ...    app=${ANDROID_APP}    appPackage=io.appium.android.apis    appActivity=.app.SearchInvoke

Input Search Query
    [Arguments]    ${query}
    Input Text    txt_query_prefill    ${query}

Submit Search
    Click Element    btn_start_search

Search Query Should Be Matching
    [Arguments]    ${text}
    wait until page contains element    android:id/search_src_text
    element text should be    android:id/search_src_text    ${text}
