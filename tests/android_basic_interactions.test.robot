*** Settings ***
Documentation     Android Basic Interactions
Resource          ../resources/resource.robot
Test Teardown     Close Application
Force Tags        Android    Interaction

*** Test Cases ***
Should send keys to search box and then check the value
    Open Android Test App    .app.SearchInvoke
    BuiltIn.Sleep    10s
    input text    txt_query_prefill    Hello world!
    BuiltIn.Sleep    10s
    click element    btn_start_search
    BuiltIn.Sleep    10s
    wait until page contains element    android:id/search_src_text
    BuiltIn.Sleep    10s
    element text should be    android:id/search_src_text    Hello world!
    BuiltIn.Sleep    10s

#Should click a button that opens an alert and then dismisses it
#    Open Android Test App    .app.AlertDialogSamples
#    Click Element    two_buttons
#    Wait Until Page Contains Element    android:id/alertTitle
#    Element Should Contain Text    android:id/alertTitle    Lorem ipsum dolor sit aie consectetur adipiscing
#    ${close_dialog_button}    get webelement    android:id/button1
#    Click Element    ${close_dialog_button}
