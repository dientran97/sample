*** Settings ***
Documentation     To test basic calculations on a Calculator App on Android
Library           AppiumLibrary
Suite Setup       Open Calculator App
Suite Teardown    Close Application
Force Tags        Android    Calculator

*** Variables ***
#Appium Configurations
${APPIUM_SERVER}    http://127.0.0.1:4723/wd/hub
${ANDROID_AUTOMATION_NAME}    UIAutomator2
${ANDROID_PLATFORM_NAME}    Android
${ANDROID_PLATFORM_VERSION}    %{ANDROID_PLATFORM_VERSION=9.0}
${DEVICE_NAME}    emulator-5554
${ANDROID_APP_PACKAGE}    com.android.calculator2
${ANDROID_APP_ACTIVITY}    com.android.calculator2.Calculator

#Element locators for varous objects on calculator app
${EQUALS_SIGN}    eq
${DISPLAYED_RESULT}    result

#Test data
${DIGIT1}         7
${DIGIT2}         9
${ADDITION_RESULT}    16
${SUBTRACTION_RESULT}    63

*** Test Cases ***
Test Addition
    [Documentation]    Tests addition of two numbers on Calculator app.
    Enter Digits And Operator    ${DIGIT1}    ${DIGIT2}    add
    View Result
    Verify Result    ${ADDITION_RESULT}

Test Multiplication
    [Documentation]    Tests multiplication of two numbers on Calculator app.
    Enter Digits And Operator    ${DIGIT1}    ${DIGIT2}    mul
    View Result
    Verify Result    ${SUBTRACTION_RESULT}

*** Keywords ***
Open Calculator App
    [Documentation]    Opens the calculator app with a new appium session.
    Open Application    ${APPIUM_SERVER}    deviceName=${DEVICE_NAME}    automationName=${ANDROID_AUTOMATION_NAME}
    ...    platformName=${ANDROID_PLATFORM_NAME}    platformVersion=${ANDROID_PLATFORM_VERSION}
    ...    appPackage=${ANDROID_APP_PACKAGE}    appActivity=${ANDROID_APP_ACTIVITY}

Enter Digits And Operator
    [Documentation]    Takes two single digit numbers and the operator as input and pressess the corresponding buttons on
    ...    the calculation keypad.
    [Arguments]    ${digit_1}    ${digit_2}    ${operator}
    Click Element    digit_${DIGIT1}
    Click Element    op_${operator}
    Click Element    digit_${DIGIT2}

View Result
    [Documentation]    Presses the equal button to view the result of the operation.
    Click Element    ${EQUALS_SIGN}

Verify Result
    [Documentation]    Verifies the result displayed with the expected answer.
    [Arguments]    ${expected_result}
    ${actual_result}    Get Element Attribute    ${DISPLAYED_RESULT}    text
    Should Be Equal    ${expected_result}    ${actual_result}
